// axios library / built-in fetch() API may also be used
// import axios from 'axios';
const axios = require( 'axios' );

// axios.get();
// axios.post();
// axios.put();
// axios.delete();

function getWorkshops() {
    return axios.get( `https://workshops-server.herokuapp.com/workshops` )
        .then( response => response.data );
};

getWorkshops()
    .then( workshops => console.log( workshops ) )
    .catch( error => console.log( error.message ) );