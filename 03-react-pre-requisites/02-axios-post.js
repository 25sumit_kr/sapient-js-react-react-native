const axios = require( 'axios' );

function addWorkshop( workshop ) {
    return axios.post(
        `https://workshops-server.herokuapp.com/workshops`,
        jQueryWorkshop,
        {
            headers: {
                'Content-Type': 'application/json'
            }
        }
    )
        .then( response => response.data );
}

const jQueryWorkshop = {
    "name": "jQuery",
    "description": "jQuery is a JavaScript library",
    "startDate": "2020-03-01T04:00:00.000Z",
    "endDate": "2020-03-03T08:00:00.000Z",
    "time": "9:30 am - 1:30 pm",
    "location": {
        "address": "Tata Elxsi, Prestige Shantiniketan",
        "city": "Bangalore",
        "state": "Karnataka"
    },
    "modes": {
        "inPerson": true,
        "online": false
    },
    "imageUrl": "https://upload.wikimedia.org/wikipedia/en/thumb/9/9e/JQuery_logo.svg/524px-JQuery_logo.svg.png"
};

addWorkshop( jQueryWorkshop )
    .then( updatedWorkshop => console.log( updatedWorkshop ) )
    .catch( error => console.log( error.message ) );