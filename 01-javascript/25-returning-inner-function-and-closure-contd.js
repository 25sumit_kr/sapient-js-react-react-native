function f() {
    let x = 1;

    return function() {
        console.log( 'x = ', x );
        x++;
    };
}

const h1 = f(); // x_1 gets created and accessible by h1
const h2 = f(); // x_2 gets created and accessible by h2
const h3 = f(); // x_3 gets created and accessible by h2

console.log( h1 === h2 ); // false
// Do h1 and he access the same - NO

// x_1 -> prints 1 the first time
h1();
h1();
h1();
// x_1 -> prints 3 the last time

// x_2 -> prints 1 the first time
h2();
h2();
h2();
h2();
// x_2 -> prints 4 the last time