const city = 'Bangalore';

switch( city ) {
    case 'Mumbai':
        console.log( 'Beach' );
        break;
    case 'Bangalore':
        console.log( 'Lalbagh' );
        break;
    case 'Delhi':
        console.log( 'India Gate' );
        break;
    default:
        console.log( 'Go anywhere' );
}

// return 
// {

// }

// The above returns nothing due to automatic semi-colon insertion after return
// return ;
// {

// }