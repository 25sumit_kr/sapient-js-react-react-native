// primitive types - number, boolean, string
let x = 1;
let weight = 63.5;

console.log( typeof x );
console.log( typeof weight );
console.log( typeof 25.75 ); // 'number'

let message = 'Hello, world';
let message2 = "Hello, world";

console.log( message );
console.log( typeof message ); // 'string'

console.log( message.length ); // 12
console.log( message[4] ); // o

let conversionRateStr = '78.25';
let conversionRateNum = parseFloat( conversionRateStr );
console.log( conversionRateNum );
console.log( typeof conversionRateNum );

let conversionRateInt = parseInt( conversionRateStr );
console.log( conversionRateInt );
console.log( typeof conversionRateInt );

let isSummer = true; // or false