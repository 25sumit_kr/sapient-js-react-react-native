console.log( sum1( 12, 13 ) );

// function declaration syntax
function sum1( x, y ) {
    const result = x + y;
    return result;
}

console.log( sum1( 12, 13 ) );

// error
// console.log( sum2( 12, 13 ) );

// function expression syntax (more like variable declaration)
// The RHS is called a "anonymous function expression"
const sum2 = function( x, y ) {
    const result = x + y;
    return result;
};

console.log( sum2( 12, 13 ) );

// ES2015 syntax (lambda C#)
// Arrow function (involves understanding "function context")
const sum3 = ( x, y ) => {
    return x + y;
};

console.log( sum3( 12, 13 ) );

// exactly same as sum3
const sum4 = ( x, y ) => x + y;
console.log( sum4( 12, 13 ) );

const square = n => n * n;
console.log( square( 12 ) );