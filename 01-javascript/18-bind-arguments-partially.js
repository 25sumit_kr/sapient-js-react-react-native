function foo( inc, units ) {
    this.age += inc;
    console.log( this.age + units );
}

const john = {
    name: 'Jonathan',
    age: 32
};

const johnPlusOneFoo = foo.bind( john, 1 );

johnPlusOneFoo( 'years' );
johnPlusOneFoo( 'months' );

const johnPlusTenFoo = foo.bind( john, 10 );

johnPlusTenFoo( 'years' );
johnPlusTenFoo( 'months' );