// We can define functions within functions
// g is a "local" function (gets created every time f is called)
function f() {
    let x = 1;

    function g() {
        console.log( 'i am g' );
    }

    g();
}

f();
f();
f();