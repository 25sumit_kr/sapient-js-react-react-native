// "Functions are first-class citizens"
function sum( numbers, transform ) {
    let result = 0;

    for( let i = 0; i < numbers.length; i++ ) {
        result += transform( numbers[i] );
    }

    return result;
}

const square = n => n * n;
const cube = n => n * n * n;

console.log( sum( [ 1, 2, 3, 4 ], square ) ); // transform = square
console.log( sum( [ 1, 2, 3, 4 ], cube ) ); // transform = cube
console.log( sum( [ 1, 2, 3, 4 ], n => n * n * n * n ) );
console.log( sum( [ 1, 2, 3, 4 ], n => n ) ); // plain sum