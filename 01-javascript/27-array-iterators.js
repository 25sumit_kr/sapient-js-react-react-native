// iterator methods
// forEach(), map(), reduce(), find(), filter()
// they all accept an "iterator" function as argument

const persons = [
    {
        "name" : "John",
        "age" : 32,
        "hours": 20,
        "emails": [
            "john@example.com",
            "john@gmail.com"
        ]
    },
    {
        "name" : "Jane",
        "age" : 28,
        "hours": 40,
        "emails": [
            "jane@example.com",
            "jane@gmail.com"
        ]
    },
    {
        "name" : "Mark",
        "age" : 30,
        "hours": 8,
        "emails": [
            "mark@example.com",
            "mark@gmail.com",
            "mark@yahoo.com"
        ]
    }
]

// forEach() -> process every item (like for loop)
persons.forEach(function( employee/*, idx, array */ ) {
    console.log( employee.name );
    // console.log( idx );
    // console.log( array.length );
});

let result;

// find() -> employees array -> first employee working on project "abc" (one item form the array)
// iterator function HAS to return true / false
result = persons.find( employee => employee.hours >= 20 );
console.log( 'result = ', result );

// filter() -> employees array -> all employees working on project "abc" (array - subset of the items of original employees array)
result = persons.filter( employee => employee.hours >= 20 );
console.log( 'result = ', result );

// map() -> employees array -> names of the employees - [ 'John', 'Jane', 'Mark' ] (array which is a transformation of the original employees array - number of items will be same)
// [ milk, eggs, bread ] -> [ milkshake, omlette, sandwich ]
// The iterator function should return the corresponding item in the generated array
result = persons.map( employee => employee.name );
console.log( result );

// reduce() -> aggregate value from the array -> total numbers of hours spent working in a week, average number of projects employees are involved in etc.
// let totalHours = 0;
// for( let ... ) {
//     totalHours = totalHors + arr[i].hours
// }
result = persons.reduce( ( acc, employee ) => {
    return {
        totalHours: acc.totalHours + employee.hours
    };
}, { totalHours: 0 } );
console.log( result );

// functional style of working with arrays
result = persons
    .filter( emp => emp.hours >= 20 )
    .map( emp => emp.name );
console.log( result );