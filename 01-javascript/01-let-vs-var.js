// number, boolean, string
// implicitly typed language

// var -> either global / function scope (but NO block scope)
// let is ES2015 (ES6) -> they have block scope
function foo() {
    if( true ) {
        var x = 1; // restricted to the function foo
        let y = 1;
    }
}

foo();

console.log( x );
console.log( y );