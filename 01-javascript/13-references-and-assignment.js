// primitive value (number in this case)
let x = 1;

// copy by value
let y = x;
y++;

console.log( x );
console.log( y );

// non-primitive values (arrays, objects, functions)
let john = {
    name: 'John'
};

// copy by reference
let jonathan = john; 

jonathan.name = 'Jonathan';
console.log( john );
console.log( jonathan );

// We will lose functions etc. (which JSON does not support)
let johnny = JSON.parse( JSON.stringify( john ) );
console.log( johnny );

johnny.name = 'Johnny';
console.log( john );
console.log( johnny );

// A better way is to use a library like lodash - https://lodash.com/docs/4.17.15#cloneDeep