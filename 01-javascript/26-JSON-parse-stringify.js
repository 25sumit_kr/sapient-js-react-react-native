class Person {
    constructor( obj ) {
        Object.assign( this, obj );
    }

    addEmail( email ) {
        this.emails.push( email );
    }
}

const personsJSON = `[
    {
        "name" : "John",
        "age" : 32,
        "emails": [
            "john@example.com",
            "john@gmail.com"
        ]
    },
    {
        "name" : "Jane",
        "age" : 28,
        "emails": [
            "jane@example.com",
            "jane@gmail.com"
        ]
    }
]`;

// string -> object / array
const persons = JSON.parse( personsJSON );

const personModels = [];
for( let i = 0; i < persons.length; i++ ) {
    personModels.push( new Person( persons[i] ) );
}

personModels[0].addEmail( 'john@yahoo.com' );

// object / array -> string
const personsUpdatedJSON = JSON.stringify( persons );

console.log( personsUpdatedJSON );