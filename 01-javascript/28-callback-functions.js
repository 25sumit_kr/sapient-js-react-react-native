// Callback pattern (~20 years this was the only way to handle result of async tasks)
function sum( x, y, callback ) {
    // async non-blocking function
    // Hey browser! Please execute the function after 3000 ms (3s)
    setTimeout(() => {
        console.log( 'inside the function' );
        
        let res = x + y;
        callback( res );
        
        // return res; // This result is available for the runtime
    }, 3000);
    console.log( 'after call to setTimeout' );

    // return undefined;
}

let result;
sum( 12, 13, ( result ) => {
    console.log( 'result = ', result );
});

console.log( sum.toString() );

console.log( setTimeout.toString() );