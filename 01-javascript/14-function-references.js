function sum( x, y ) {
    return x + y;
}

// add and sum refer to the SAME function in memory
// copy by reference
// functions ARE objects
const add = sum;

console.log( add( 12, 13 ) );

function celebrateBirthday() {
    this.age++;
}

const john = {
    name: 'John',
    age: 32,
    // celebrateBirthday: celebrateBirthday
    cb: celebrateBirthday,
    // "this" -> function context
    setName( newName ) {
        console.log( "this = ", this );
        this.name = newName;
    }
};

john.cb();
john.setName( 'Jonathan' ); // "this" is john object
console.log( 'john.setName( "jonathan" ) : ', john ); 

const setName = john.setName;

setName( 'Johnny' ); // "this" is NOT john object
console.log( 'setName( "johnny" ) : ', john );