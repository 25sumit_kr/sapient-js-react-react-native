// "Module pattern" -> can be used to get similar functionality as "private variables and methods" for objects
const john = {
    name: 'Jonathan',
    age: 32,
    celebrateBirthday( inc, units ) {
        this.age += inc;
        console.log( this.age + units );
    }
};

const jane = {
    name: 'Jane',
    age: 28
};

// jane can "borrow" john.celebrateBirthday
john.celebrateBirthday.call( jane, 1, 'year' );

console.log( jane );