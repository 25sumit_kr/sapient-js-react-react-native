// f can make use of g!
function f( arg ) {
    console.log( 'i am f' );
    console.log( arg );

    if( typeof arg === 'function' ) {
        arg();
    }
}

function g() {
    console.log( 'i am g' );
}

let x = 1;
f( x ); // arg = x (x is primitive - hence copy by value)
f( g ); // arg = g (g is a function, i.e. non-primitive - hence copy by reference)