// john = ... // does not work
// bag of properties
// object are dynamic in JS -> we can add / delete properties
// key is always a string, value can be anything
const john = {
    "name": 'John',
    spouse: 'Jane',
    emails: [
        'john@gmail.com',
        'john@example.com'
    ],
    address: {
        city: 'Bangalore',
        region: 'Bangalore South'
    }
};

john.age = 32;
delete john.spouse;

console.log( john );
console.log( john.address.city );
console.log( john.emails[1] );

console.log( john.age );

// useful for accessing any property at runtime
let key = 'age';
console.log( john[key] );

// accessing beyond last item in an array, access non-existent property on object
console.log( john.children ); // undefined

// Cannot read property '0' of undefined (i.e. john.children is undefined)
console.log( john.children[0] ); // undefined[0]
