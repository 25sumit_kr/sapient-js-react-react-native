// do..while, while
for( let i = 0; i < 10; i++ ) {
    console.log( i );
}

// ES2015 -> for of (iterable objects -> arrays, strings, Set, Map)
for( const char of 'Hello, world' ) {
    console.log( char );
}