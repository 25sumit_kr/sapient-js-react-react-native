// Arithmetic operators (+, -, *, /, %, ** -> exponentiation)
console.log( 1 / 2 );
console.log( 3.5 ** 4.5 );

// Relational operator (=== is special, x !== y <=> !( x === y ))
console.log( 1 < 2 );

console.log( 1 == 1 ); // true
console.log( 1 === 1 ); // true

console.log( 1 == '1' ); // true
console.log( 1 === '1' ); // false (type-safe equality check)

// objects are equal only if they refer to the SAME object in memory
// We create 2 objects in memory
let john = {
    name: 'John'
};

let jonathan = {
    name: 'John'
};

let johnny = john; // john and johnny refer to the same object in memory

console.clear();

console.log( john == jonathan );
console.log( john === jonathan );
console.log( john == johnny );
console.log( john === johnny );

// Boolean - !, &&, ||
// cond ? true_value : false_value

// range
console.log( Number.MAX_SAFE_INTEGER );
console.log( Number.MIN_VALUE );

// equality objects - no built-in JS way to do this
// we can use lodash utilities - https://lodash.com/docs/4.17.15#isEqual
