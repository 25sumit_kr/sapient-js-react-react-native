let x = 1;
console.log( 'x in global space = ', x );

function f() {
    let y = 2, z = 3;

    console.log( 'x in f space = ', x );
    console.log( 'y in f space = ', y );
    console.log( 'z in f space = ', z );

    // inner function's can access all variables in the "enclosing scope" where they are created
    function g() {
        let z = 4;
        console.log( 'x in g space = ', x );
        console.log( 'y in g space = ', y );
        console.log( 'z in g space = ', z );
    }

    g();
}

f();