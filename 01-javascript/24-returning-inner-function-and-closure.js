// We can define functions within functions
// g is a "local" function (gets created every time f is called)
function f() {
    let x = 1; // the lifetime of this variable extends beyond the life of the f function call!

    // closure of g contains the variables of g + variables in the enclosing scope
    function g() {
        let y = 2;

        console.log( 'i am g' );
        console.log( 'x = ', x );
        console.log( 'y = ', y );

        x++;
    }

    return g;
}

const h = f(); // h = g (both h and g refer to the same function in memory)

h(); // calls g -> g is able to make use of x (it is still in memory)
h();
h();