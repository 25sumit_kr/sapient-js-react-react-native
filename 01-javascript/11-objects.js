const john = {
    "name": 'John',
    spouse: 'Jane',
    emails: [
        'john@gmail.com',
        'john@example.com'
    ],
    address: {
        city: 'Bangalore',
        region: 'Bangalore South'
    },
    celebrateBirthday: function() {
        // "this" -> object to which this method belongs
        // CAUTION: "this" NEED NOT refer to the object
        this.age++;
    }
};

// assigning a new property age
john.age = 32;

// can add methods later on as well
john.changeCity = function( newCity ) {
    this.city = newCity;
};

john.celebrateBirthday();
john.changeCity( 'Mumbai' );
console.log( john );