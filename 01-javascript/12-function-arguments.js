function foo( x, y, z ) {
    console.log( x );
    console.log( y );
    console.log( z );
    console.log( z + x ); // results in NaN when undefined is involved
}

foo( 15, 20, 25 );
foo( 15, 20 ); // x -> 15, y -> 20, z -> undefined
foo( 15, 20, 25, 30 ); // x -> 15, y -> 20, z -> 25, we can make use of other arguments using the "Rest operator" (ES2015 feature)