// const variables cannot be reassigned
// the array remains mutable!
let numbers = [ 12, 13, 14, 15, 16, 'Seventeen', [ 'Eighteen', 19 ] ];
numbers[0] = numbers[0] + 10;

numbers.push( 20 );
console.log( numbers );
console.log( numbers.length ); // number of items

console.log( typeof numbers ); // object
numbers = 12;
console.log( typeof numbers ); // number

const matrix = [
    [ 1, 2, 3 ],
    [ 4, 5, 6, 7 ],
    [ 8, 9 ]
];

console.log( matrix[1][2] ); // 6

// freeze works for objects
let john = {
    name: 'John'
};

Object.freeze( john );

john.name = 'Jonathan';

console.log( john );

// freeze is not working for array
let numbers2 = [ 10, 11, 12, 13, 14 ];

// numbers2 array is immutable after this executes
Object.freeze( numbers2 );

numbers2.push( 15 ); // silently fails to insert new item
console.log( numbers2 ); // remains same
