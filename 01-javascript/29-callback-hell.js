function sum( x , y, cb ) {
    setTimeout( () => cb( x + y ), 2000 );
}

// unreadable
// callback hell
sum( 12, 13, result => {
    console.log( 'sum( 12, 13 ) = ', result );

    sum( result, 14, result2 => {
        console.log( 'sum( previous result, 14 ) = ', result2 );

        sum( result2, 15, result3 => {
            console.log( 'sum( previous result, 15 ) = ', result3 );
        });
    });
});