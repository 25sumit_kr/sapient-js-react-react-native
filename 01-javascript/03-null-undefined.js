let x; // undefined (type is also called undefined)
console.log( x );
console.log( typeof x );

let y = undefined; // we can use undefined explicitly this way
console.log( y );

let obj = null; // a missing object value
console.log( obj );

obj = {
    name: 'John',
    age: 32
};
console.log( obj );

obj = null; // the object pointed to is eligible for garbage collection

if( x ) { // false -> undefined | null | false | 0 | '' ("falsy")
    console.log( 'Truthy' );
} else {
    console.log( 'Falsy' );
}

let z = null; // undefined

// Prefer using === 
if( z === undefined ) {
    console.log( 'z is undefined' );
} else if( z === null ) {
    console.log( 'z is null' );
}