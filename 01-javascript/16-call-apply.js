function foo( x, y ) {
    console.log( "x, y = ", x, y );
    console.log( this );
    this.age++;
}

const john = {
    name: 'Jonathan',
    age: 32
};

// change the context for this 1 call to foo
// foo.call() calls foo() with context john
foo.call( john, 12, 13 );
foo.apply( john, [ 12, 13 ] );

console.log( john );