const john = {
    "name": 'John',
    age: 32,
    spouse: 'Jane',
    emails: [
        'john@gmail.com',
        'john@example.com'
    ],
    children: [
        'Jack',
        'Jill'
    ],
    address: {
        city: 'Bangalore',
        region: 'Bangalore South'
    }
};

const johnCopy = {
    ...john,
    emails: [ ...john.emails ],
    children: [ ...john.children ],
    address: {
        ...john.address   
    }
};

johnCopy.emails.push( 'john@sapient.com' );
johnCopy.emails[0] = 'john@abc.com';
johnCopy.address.city = 'Mumbai';

console.log( john );
console.log( johnCopy );