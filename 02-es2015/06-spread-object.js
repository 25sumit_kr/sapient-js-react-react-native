// spread scenario 2 : along with object
const john = {
    "name": 'John',
    age: 32,
    spouse: 'Jane',
    emails: [
        'john@gmail.com',
        'john@example.com'
    ],
    children: [
        'Jack',
        'Jill'
    ],
    address: {
        city: 'Bangalore',
        region: 'Bangalore South'
    },
    celebrateBirthday: function() {
        // "this" -> object to which this method belongs
        // CAUTION: "this" NEED NOT refer to the object
        this.age++;
    }
};

const johnProject = {
    name: 'Jonathan',
    project: 'ABC',
    hours: 20
};

// ...john // name : john.name (proper copy), spouse: john.spouse (proper copy), emails: john.emails, ......
const johnCopy = {
    ...john,
    ...johnProject, // name: johnProject.name, project: johnProject.project, hours: johnProject.hours
    children: [
        'Dennis',
        'Diana'
    ]
};

johnCopy.age++; // does not affect john.age
johnCopy.emails.push( 'john@yahoo.com' ); // affects john.emails

console.log( john );
console.log( johnCopy );