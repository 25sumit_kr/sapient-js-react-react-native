// Arrow functions help us access the enclosing function context
function outer() {
    console.log( 'this (outer) = ', this );

    const inner1 = function() {
        console.log( 'this (inner1 - old syntax) = ', this );
    };
    
    // there is no "this" for an arrow function -> "this" is actually the outer function "this"
    const inner2 = () => {
        console.log( 'this (inner2 - arrow function syntax) = ', this );
    };

    inner1();
    inner2();
}

outer.call( { x: 1 } );