function * counter( seed, end ) {
    let count = seed;
    
    while( true ) {
        if( count === end ) {
            return count;
        } else {
            yield count
        }

        count++;
    }
}

const counter1 = counter( 100, 200 );

const counter2 = counter( 10000, 10100 );

for( let id of counter1 ) { // counter1.next().value and continues till counter1.next() become true
    console.log( id );
}

for( let id of counter2 ) {
    console.log( id );
}