// generator -> a function that can be "paused" and "resumed"
// "pause" -> the function will save the state (its local variables), give up control (and "yield" some intermediate value)
function * foo( x ) {
    console.log( 'x = ', x );
    console.log( 1 );

    let input;
    
    input = yield 200;

    console.log( 'input = ', input );
    console.log( 2 );

    input = yield 201;

    console.log( 'input = ', input );
    console.log( 3 );

    return 202;
}

const iter = foo( 100 );
let result;

result = iter.next( 101 ); // the function starts execution now
console.log( result );

result = iter.next( result.value + 1000 ); // the function starts execution now
console.log( result );

result = iter.next( result.value + 1000 );
console.log( result );