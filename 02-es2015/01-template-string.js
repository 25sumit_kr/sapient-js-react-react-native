const message = 'Good morning', john = {
    name: 'John',
    city: 'Bangalore',
    getCity() {
        return this.city;
    }
};

// const welcomeMessage = message + ' ' + john.name + '! How is the weather in ' + john.city + '?';
// back-tick quoted strings support interpolation and preserve whitespace characters including newline
const welcomeMessage = `${message} ${john.name}!
How is the weather in ${john.getCity()}?`;

console.log( welcomeMessage );