class Movie {
	constructor( name, casts, yearOfRelease, boxOfficeCollection) {
		this.name = name;
		this.casts = casts;
		this.yearOfRelease = yearOfRelease;
		this.boxOfficeCollection = boxOfficeCollection;
	}

	addToCast(newMember) {
		this.casts.push(newMember)
	}

	addToCollection(amount){
	    this.boxOfficeCollection += amount
	}
}

const ironman = new Movie( 'John', ['Tonny','Johhy'],2003,233 ); 
ironman.addToCast("bunty");
ironman.addToCollection(10);

const jronman = new Movie( 'John', ['Tonny','Johhy'],2003,233 ); 
jronman.addToCast("bunty");
jronman.addToCollection(10);

console.log( ironman, jronman );