class Person {
    constructor( name, age ) {
        this.name = name;
        this.age = age;
    }

    celebrateBirthday() {
        this.age++;
    }
}

class Employee extends Person {
    static nationality = 'Indian';
    
    static sayHello() {
        console.log( 'Hello' );
    }

    constructor( name, age, role, dept ) {
        super( name, age );

        this.role = role;
        this.dept = dept;
    }

    promote() {
        this.role = `Senior ${this.role}`;
    }

    celebrateBirthday() {
        super.celebrateBirthday();
        console.log( `Happy birthday ${this.name}` );
    }
}

const john = new Employee( 'John', 32, 'Accountant', 'Finance' );
john.celebrateBirthday();
john.promote();
console.log( john );

Employee.sayHello();