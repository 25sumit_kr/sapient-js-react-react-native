function sum( x , y ) {
    return new Promise(( a, reject ) => {
        if( typeof x !== 'number' || typeof y !== 'number' ) {
            reject( new Error( 'at least one argument was not a number' ) );
            return;
        }

        setTimeout( () => {
            a( x + y );
        }, 2000 );
    });
}

// const result = sum( 12, 13 )

// async function ALWAYS return a Promise
async function doSerialCalls() {
    try {
        console.log( 1 );

        let result;
        
        result = await sum( 12, 13 ); // "yield" here
        console.log( result );

        result = await sum( result, 14 ); // "yield" here
        console.log( result );
        
        result = await sum( result, 15 ); // "yield" here
        console.log( result );

        return result; // wrapped in aPromise object that resolves with result
    } catch( error ) {
        console.log( error.message );
    }
}

doSerialCalls()
    .then( result => {
        console.log( 'final result = ', result );
    })
    .catch( error => {
        console.log( error.message );
    })

console.log( 2 );