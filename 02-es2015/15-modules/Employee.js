import { Person } from './Person.js';

// you can have ONLY 1 default export in a module
/*export default */class Employee extends Person {
    static nationality = 'Indian';
    
    static sayHello() {
        console.log( 'Hello' );
    }

    constructor( name, age, role, dept ) {
        super( name, age );

        this.role = role;
        this.dept = dept;
    }

    promote() {
        this.role = `Senior ${this.role}`;
    }

    celebrateBirthday() {
        super.celebrateBirthday();
        console.log( `Happy birthday ${this.name}` );
    }
}

const sum = ( x, y ) => x + y;

// "named" exports
/*export */const diff = ( x, y ) => sum( x, -y);
/*export */const mult = ( x, y ) => x * y;

const divide = ( x, y ) => x / y;

export {
    Employee as default,
    diff,
    mult
}