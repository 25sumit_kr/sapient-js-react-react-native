import Emp, { diff, mult } from './Employee.js';

const john = new Emp( 'John', 32, 'Accountant', 'Finance' );
john.celebrateBirthday();
john.promote();
console.log( john );

console.log( diff( 12, 13 ) );
console.log( mult( 12, 13 ) );

Emp.sayHello();