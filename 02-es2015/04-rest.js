// Rest (3 scenarios) / Spread (2 situations) : ...
// React also has "props spread" (1 situation)

// scenario 1: along with last argument of a function
// gather rest of the arguments into an array
function foo( x, y, ...z ) {
    console.log( x );
    console.log( y );
    console.log( z );
}

foo( 10, 11, 12, 13, 14 );
foo( 10 );

// scenario 2: during array destructuring to gather the items AFTER the LAST destructured item
const days = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday'
];

console.clear();

// gather items after the last properly destructured item (second) into an array
const [ first, second, ...restOfDays ] = days;
console.log( first, second, restOfDays );

// scenario 3: during object destructuring to gather into a new object the key-value pairs that are NOT part of the destructuring
const john = {
    "name": 'John',
    spouse: 'Jane',
    emails: [
        'john@gmail.com',
        'john@example.com'
    ],
    address: {
        city: 'Bangalore',
        region: 'Bangalore South',
        pinCode: 560045
    },
    celebrateBirthday: function() {
        // "this" -> object to which this method belongs
        // CAUTION: "this" NEED NOT refer to the object
        this.age++;
    }
};

console.clear();

const {
    name,
    address : { city, ...restOfAddress },
    ...restOfJohnDetails
} = john;

console.log( restOfJohnDetails );
console.log( restOfAddress );