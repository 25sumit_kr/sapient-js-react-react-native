function sum( x , y ) {
    return new Promise(( a, reject ) => {
        if( typeof x !== 'number' || typeof y !== 'number' ) {
            reject( new Error( 'at least one argument was not a number' ) );
            return;
        }

        setTimeout( () => {
            a( x + y );
        }, 2000 );
    });
}

// like a try..catch
sum( 12, 13 )
    .then( result => {
        console.log( 'result = ', result );
        return sum( result, 14 );
    })
    .then( value => { // gets called after sum( result, 14 ) is resolved
        console.log( 'value = ', value );
        foo(); // no such function
    })
    .catch( error => {
        console.log( 'error.message = ', error.message );
    });