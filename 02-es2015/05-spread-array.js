// ... (spread)

// scenario 1 : along with an array
const nums1 = [ 1, 2, 3 ];
// ...num1 // numbers[0], numbers[1], numbers[2]
const nums1Copy = [ ...nums1 ]; // [ numbers[0], numbers[1], numbers[2] ] -> items are copied by value (since they are numbers) 

nums1Copy[0]++;

console.log( nums1 );
console.log( nums1Copy );

// spread with array of object
const persons = [
    { name: 'John', age: 32 },
    { name: 'Jane', age: 28 },
    { name: 'Mark', age: 40 }
];

const personsCopy = [ ...persons ]; // [ persons[0], persons[1], persons[2] ] -> items arecopied by reference (items are objects)

personsCopy[0].age++;
personsCopy.push({
    name: 'Mary',
    age: 44
});

console.log( persons );
console.log( personsCopy );

// const nums1 = [1,2,3,4];
// const nums1Copy = [...nums1]; // proper copy
// nums1Copy[2]=55; // [ 1, 2, 55, 4 ] in nums1Copy
// nums1Copy[3]++; // [ 1, 2, 55, 5 ] in nums1Copy
// console.log(nums1); // [1,2,3,4];
// console.log(nums1Copy); // [ 1, 2, 55, 5 ]

const personsDeepCopy = persons.map( person => {
    // ...person -> name: person.name, age: person.age 
    // since both are primitives, the values are proper copies
    return {
        ...person
    };
});

personsDeepCopy[0].age++;

console.clear();

console.log( persons );
console.log( personsDeepCopy );