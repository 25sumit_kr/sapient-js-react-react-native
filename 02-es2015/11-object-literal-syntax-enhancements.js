const name = 'John';
const emails = [
    'john@gmail.com'
];
const spouse = 'wife';

const john = {
    name, // name: name
    age: 32,
    emails, // emails: emails
    [spouse]: 'Jane',
    celebrateBirthday() {
        this.age++;
    }
};

// john[spouse] = 'Jane'; // only way in old JS to set up a key from a variable

console.log( john );