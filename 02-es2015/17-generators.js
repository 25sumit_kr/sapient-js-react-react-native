// generator -> a function that can be "paused" and "resumed"
// "pause" -> the function will save the state (its local variables), give up control (and "yield" some intermediate value)
function * foo( x ) {
    console.log( 'x = ', x );
    console.log( 1 );

    // return undefined; // -> { done: true, value: undefined }
}

const iter = foo( 100 );
let result;

result = iter.next( 101 ); // the function starts execution now
console.log( result );

result = iter.next( 101 ); // the function starts execution now
console.log( result );