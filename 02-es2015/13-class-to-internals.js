class Person {
    constructor( name, age ) {
        this.name = name;
        this.age = age;
    }

    celebrateBirthday() {
        this.age++;
    }
}





// function Person( name, age ) {
//     this.name = name;
//     this.age = age;
// }

// Person.prototype.celebrateBirthday() {
//     this.age++;
// }