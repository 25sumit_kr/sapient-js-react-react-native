function sum( x , y ) {
    return new Promise(( a, reject ) => {
        if( typeof x !== 'number' || typeof y !== 'number' ) {
            reject( new Error( 'at least one argument was not a number' ) );
            return;
        }

        setTimeout( () => {
            a( x + y );
        }, 2000 );
    });
}

// function foo() {
    // return sum( 12, 'hello' )
    //     .then( result => {
    //         console.log( 'result = ', result );
    //         return 100;
    //     })
    //     .then( value => {
    //         console.log( 'value = ', value );
    //     })
    //     .catch( error => {
    //         console.log( 'error.message = ', error.message );
    //     });
// }

// foo()
//     .then()

sum( 12, 13 )
    .then( result => {
        console.log( 'result = ', result );
        return 100;
    })
    .then( value => {
        console.log( 'value = ', value );
    })
    .catch( error => {
        console.log( 'error.message = ', error.message );
    });

    // const promise = sum(12,'a');
    // // function passed to then() gets called when promise reaches resolved state
    // promise.then(value => {
    //    console.log(value);
    // }).catch(reason => console.log("Got an error!"));
    // console.log("Continuing further");