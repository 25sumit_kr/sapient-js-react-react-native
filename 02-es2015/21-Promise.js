// "Promise" (built-in class introduced in ES2015) is an intermediary
function sum( x , y ) {
    // a Promise object can be in 1 / 3 states - initially in "unfulfilled" state. Then it can go into either "resolved" state / "rejected" state
    return new Promise(( a, reject ) => {
        if( typeof x !== 'number' || typeof y !== 'number' ) {
            reject( new Error( 'at least one argument was not a number' ) ); // the promise "rejects"
            return;
        }

        setTimeout( () => {
            a( x + y ); // Hey Promise object! Here is the result (the promise "resolves")
        }, 2000 );
    });
}

const promise = sum( 12, 'hello' );

// the function passed to catch() is called when the promise is rejected
const v = promise.catch( error => {
    console.log( error.message );
}); // promiseA resolves

console.log( v instanceof Promise ); // true (catch() returns a Promise)

// the function passed to then() is called when the promise is resolved
promise.then( result => { // then() too returns a Promise
    console.log( 'result = ', result );
}); // when promise rejects, promiseB will reject too

// console.log( 'last line in the script' );