// iterables (implement the iterator interface)
// Array, string, Map, Set

const numbers = [ 12, 13, 14, 15 ];

// for..of loop
for( let item of numbers ) {
    console.log( item );
}

for( let char of 'hello' ) {
    console.log( char );
}