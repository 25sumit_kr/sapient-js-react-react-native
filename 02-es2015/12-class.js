class Person {
    // class properties syntax
    // similar to assigning this.nationality = 'Indian' within the constructor
    nationality = 'Indian';

    constructor( name, age, emails ) {
        console.log( this );

        this.name = name;
        this.age = age;
        this.emails = emails;

        // return this;
    }

    celebrateBirthday() {
        console.log( this );
        this.age++;
    }
    
    // class properties syntax
    // creates a bound method for every object
    celebrateBirthday2 = () => {
        console.log( this );
        this.age++;
    }
}

const john = new Person( 'John', 32, [ 'john@gmail.com' ] ); // an empty object {} is created
john.celebrateBirthday();
console.log( john );

const jane = new Person( 'Jane', 28 );
console.log( jane );

const cb = john.celebrateBirthday; // they refer to same function in memory
// cb(); // "this" -> undefined (Node.js) -> throws error | window (browser)

const cb2 = john.celebrateBirthday.bind( john );
cb2(); // increases john's age
console.log( john );

const cb3 = john.celebrateBirthday2;
cb3(); // increases john's age
console.log( john );