const john = {
    "name": 'John',
    spouse: 'Jane',
    emails: [
        'john@gmail.com',
        'john@example.com'
    ],
    address: {
        city: 'Bangalore',
        region: 'Bangalore South'
    },
    celebrateBirthday: function() {
        // "this" -> object to which this method belongs
        // CAUTION: "this" NEED NOT refer to the object
        this.age++;
    }
};

// const name = john.name;
const {
    name : firstName,
    age = 18,
    address : { city },
    emails : [ , secondEmailId ]
} = john;
console.log( firstName, age, city, secondEmailId );

// We can destructure the object and extract properties into variables in the argument itself
function ajax( { method, url } ) {
    // const { method, url } = options;

    console.log( method );
    console.log( url );
}

ajax({
    method: 'GET',
    url: 'http://www.example.com'
});