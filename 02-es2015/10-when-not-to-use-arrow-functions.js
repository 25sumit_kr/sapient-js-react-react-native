// DO NOT use arrow function in objects
const john = {
    name: 'John',
    age: 32,
    celebrateBirthday: () => {
        // "this" is the global object, not john
        this.age++;
    }
};